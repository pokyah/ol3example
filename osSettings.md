## Voir où se trouve le fichier permettant de créer des virtual hsots :

```bash
$ /etc/apache2> gedit httpd.conf
```

Ce fichier indique le chemin suivant : 

```
IncludeOptional /etc/apache2/vhosts.d/*.conf

```

## Créer un fichier de virtual host

1. Créer le fichier de configuration de virtual host
```bash 
$ cd /etc/apache2/vhosts.d/
$ vi formation.conf
```
2. Coller dans ce fichier le code suivant :

```
WSGIScriptAlias WSGIScriptAlias /data /home/t.goossens/Documents/agrometDev/JS-FormationOslandia/api/api.wsgi
<Directory /home/sbe/projects/formation_gembloux/api>
    <Files api.wsgi>
        #Order deny,allow
        #Allow from all
        WSGIScriptReloading On
        #WSGIPassAuthorization On
    </Files>
</Directory>
```
Ce fichier spécifie qu'en alalnt dans localhost/data, on va chercher ce qu'il y a à 

```
/home/t.goossens/Documents/agrometDev/JS-FormationOslandia/api/api.wsgi
```

3. restart apache
```bash
$ sudo apache2ctl restart
```

4. installer l'extension manquante apache2-mod_wsgi
```bash
$ sudo apache2ctl restart
```