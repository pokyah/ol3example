var FG = FG || {};

FG.main = {
    map: null,
    osmLayer: null,
    view: null,
    inter: null,
    /*
     *  Init the whole process
     */
    init: function() {
        this.initMap();
        this.linkControls();
    },

    initMap: function() {
        var scaleLine = new ol.control.ScaleLine();
        this.osmLayer = new ol.layer.Tile({
            source: new ol.source.OSM()
        });
        this.nasaLayer = new ol.layer.Tile({
            source: new ol.source.TileWMS({
                title: 'Nasa BlueMarble',
                url: 'https://ahocevar.com/geoserver/wms',
                params: {
                    LAYERS: 'nasa:bluemarble',
                    TILED: true
                }
            })
        });
        this.nasaLayer.setVisible(false);
        this.batiSource = new ol.source.Vector();
        this.batiLayer = new ol.layer.Vector({
            source: this.batiSource,
            style: FG.main.styleFn
        });

        this.drawSource = new ol.source.Vector();
        this.drawLayer = new ol.layer.Vector({
            source : this.drawSource
        });

        this.map = new ol.Map({
            target: 'map',
            layers: [
               this.osmLayer,
               this.nasaLayer,
               this.batiLayer,
               this.drawLayer
            ],
            view: new ol.View({
                center: ol.proj.transform([2.3, 48.9], 'EPSG:4326', 'EPSG:3857'),
                zoom: 10
            }),
            controls: ol.control.defaults().extend([scaleLine])
        });
        this.view = this.map.getView();
        //FG.main.map.addLayer(FG.main.batiLayer);
        this.loadBatis();

    },

    /*
     * Style for features
     */
    styleFn: function(feature) {
        /*var color = "rgba(255, 0, 0, 0.3)";
        if(feature.getProperties().nature.trim() == "Bâtiment sportif"){
            color = "rgba(0, 0, 255, 0.5)";
        }*/

        var hauteur = feature.getProperties().hauteur;
        var color = chroma('red').brighten(hauteur).hex();

        var style = new ol.style.Style({
            stroke: new ol.style.Stroke({
                color: 'orange',
                width: 1
            }),
            fill: new ol.style.Fill({
                color: color
            })
        });
        return style;
    },

    /*
     * Link the controls to code
     */
    linkControls: function() {
        $('#osmLink').click(this.enableOsm);
        $('#nasaLink').click(this.enableNasa);
        $('#selectType').change(this.setSelectionType);
        $('#searchAddress').click(this.searchAddress);
    },

    /*
    * localize map
    */
    searchAddress: function(){
        var address = $('#address').val();
        console.log('gggtgt');
        $.getJSON('https://nominatim.openstreetmap.org/search?format=json&q='+address,
        function(data){
            var lon = data[0].lon;
            var lat = data[0].lat;
            console.log(lat);
            FG.main.view.setCenter(
                ol.proj.transform([parseFloat(lon),parseFloat(lon)], 'EPSG:4326', 'EPSG:3857')
            )

        });
    },

    /*
    * set Selection type
    */
    setSelectionType: function(o) {
        var selectType = o.target.value;
        FG.main.map.removeInteraction(FG.main.inter);

        if (selectType == 'point'){
            FG.main.drawSource.clear();
            FG.main.inter = new ol.interaction.Select({
                condition: ol.events.condition.click
            });
            FG.main.map.addInteraction(FG.main.inter)
            FG.main.inter.on('select', function(e){
                var features = e.target.getFeatures();
                features.forEach(function(f){
                    var fid = f.getProperties().ogc_fid;
                    var nature = f.getProperties().nature;
                    $('#data').html(fid + "-" + nature);
                });
                $("#popupBtn").trigger("click");
            })
        }
        if (selectType == 'polygon'){
            FG.main.inter = new ol.interaction.Draw({
                source : FG.main.drawSource, //couche de stockage des dessins
                type : ('Polygon') // on force le dessin type polygone
            });
            FG.main.map.addInteraction(FG.main.inter);

            FG.main.inter.on('drawend', function(e){
                var formatWKT = new ol.format.WKT();
                var wkt = formatWKT.writeGeometry(e.feature.getGeometry());
                $.post("http://192.168.10.146/data/batis_extent",{
                    wkt: wkt
                },
                function(data, status){
                    console.log(data);
                })
                
            });

            FG.main.inter.on('drawstart', function(e){
                FG.main.drawSource.clear();
            });
        }
    },


    /*
     * Load bati data
     */ 
    loadBatis: function() {
        /*
        On peut charger la couche ici, ou dans initMap, peu importe
        FG.main.batiSource = new ol.source.Vector();
        FG.main.batiLayer = new ol.layer.Vector({
        source: FG.main.batiSource
        });
        */
        /*
        $.ajax({
            url: 'data/batis.json',
            dataType: 'json',
            success: function(data) {
                var geojsonFormat = new ol.format.GeoJSON();
                FG.main.batiSource.addFeatures(geojsonFormat.readFeatures(data));
                FG.main.map.addLayer(FG.main.batiLayer);
            }
        });
        */
        $.get("data/batis.json", function(data, status){
        //$.get("http://192.168.10.146/data/batis", function(data, status){//cal a file on another machine   
            var geojsonFormat = new ol.format.GeoJSON();
            FG.main.batiSource.addFeatures(geojsonFormat.readFeatures(data));
            //FG.main.map.addLayer(FG.main.batiLayer);
            var features = FG.main.batiSource.getFeatures();
            var myContent = "<table class='table data-table'>";
            for( var i=0; i < features.length; i++){
                var f = features[i];
                var id = f.getProperties().ogc_fid;
                var nature = f.getProperties().nature;
                myContent += "<tr onclick='FG.main.zoomOnFeature("+id+")'><td>"+id+"</td><td>"+nature+"</td></tr>";
            };
            myContent += "</table>";
            $("#tabData").html(myContent);
        });
    },

    /*
    * Zoom on single Feature
    */
    zoomOnFeature: function(fid) {
        var features = FG.main.batiSource.getFeatures();
        for(var i=0; i < features.length; i++){
            var f = features[i];
            var id = f.getProperties().ogc_fid;
            if(fid == id){
                var extent = f.getGeometry().getExtent();
                FG.main.view.fit(extent, FG.main.map.getSize());
                FG.main.view.setZoom(FG.main.map.getView().getZoom()-1);
                
                break;
            }
        }
    },

    /*
     * Set the osm layer visible
     */
    enableOsm: function() {
        FG.main.hideLayers();
        FG.main.osmLayer.setVisible(true);
    },
    /*
     * Set the nasa layer visible
     */
    enableNasa: function() {
        FG.main.hideLayers();
        FG.main.nasaLayer.setVisible(true);
    },
    /*
     * Hide all layers
     */
    hideLayers: function() {
        var layers = FG.main.map.getLayers().getArray();
        for(var l = 0 ; l < layers.length ; l++) {
            var curLayer = layers[l];
            curLayer.setVisible(false);
        }
    }    
}

$(function() {
    FG.main.init();
});